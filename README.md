# Custom Implementation of Azerbaijan's First Bug Bounty Platform in MERN Stack 

## Description: What is Bug Bounty?
A bug bounty program, also called a **vulnerability rewards program** (VRP), is a **crowdsourcing** initiative that rewards individuals for discovering and reporting **software bugs**. Bug bounty programs are often initiated to supplement internal code audits and penetration tests as part of an organization's vulnerability management strategy. <br /><br />
Many software vendors and websites run ***bug bounty programs***, paying out cash rewards to software security researchers and white hat hackers who report software vulnerabilities that have the potential to be exploited. Bug reports must document enough information for for the organization offering the bounty to be able to reproduce the vulnerability. Typically, payment amounts are commensurate with the size of the organization, the difficulty in hacking the system and how much impact on users a bug might have.

## Installation and Run
To install and run this MERN app on your local machine you will need to do bunch of steps first :
- [Download and install node.js and npm](#download-and-install-nodejs-and-npm)
- [Clone needed repositories to your computer](#clone-needed-repositories-to-your-computer)
- [Install and Deploy Backend in your local machine](#install-and-deploy-backend-in-your-local-machine)
- [Install and Deploy Frontend in your local machine](#install-and-deploy-frontend-in-your-local-machine)
- [Run the app on your machine](#run-the-app-on-your-machine)


## Download and install node.js and npm
### Check if it's already installed
Firstly, if there is a chance of node.js and npm being already installed on your computer, check them by :
```
node --version
npm --version
```
So if these output you anything, like *v12.16.1* or *6.5.1*, it means you already have node.js installed on your machine.

If not, please do the below steps to install it properly.

### For Windows machines
If you have Windows as your OS, you can download and install node.js easily on
> https://nodejs.org/en/download/

make sure you choose **windows installer**.

### For Linux (Unix) machines
One of most ways to install node.js and npm in your linux machine, is to install it by [NVM](https://github.com/nvm-sh/nvm) (Node Version Manager).We suggest you to do it this way as it is a practical tool for managing multiple Node.js versions. 
#### 1. To install NVM, download the installation script from Github.For that, you will use the curl command line.
   - If you do not have `curl`, install it by running:
   
   - `sudo apt install curl`
   
   - Press **y** to confirm the installation and hit **Enter**
   
#### 2. Now, download the NVM installation script with the command:
   - `curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash`
   - After it automatically clones the NVM repository and adds the NVM path to ZSH or Bash, you will receive the        following output:
   
![Couldn't found image](https://phoenixnap.com/kb/wp-content/uploads/2019/03/download-nvm-installation-script.png   'NVM post-installation output')

#### 3. To **enable nvm**:
   - Close and open the terminal or
   - Run the given command in the above figure

#### 4. Check whether the installation was successful by verifying `nvm` version:
   - `nvm --version`
   
   - Once you have installed **`nvm`**, you can find a list of all the available Node.js versions with the command: 
   
   - `nvm ls-remote`
   
   - This will list all available versions of **`nvm`**
   
![Couldn't found image](https://phoenixnap.com/kb/wp-content/uploads/2019/03/list-available-nvm-versions.png 'Output of the comment nvm ls-remote')

#### 5. Finally, install a specific version
   - NVM is a package manager; it can install and manage multiple Node.js versions.
   
   - To install a particular version, use the command `nvm install` and add the number of the version.
   
   - For the sake of this particular project, we will use version **12.16.1**
   
   - To install, just type the following command:
   
   - `nvm install 12.16.1`
   
So that's it.Now you have node and npm installed on your machine.You can check the versions by typing:
- **`node --version`**
- **`npm --version`**



## Clone needed repositories to your computer
Next step is cloning the both back-end and front-end part of the site to your machine to get this MERN app installed on your computer. <br /> <br />
You can clone ***back-end*** with `git clone` <br />
`git clone https://gitlab.com/VusalIsm/bug_bounty.git` <br />
also ***front-end***, with <br />
`git clone https://gitlab.com/VusalIsm/bug_bounty_client.git`



## Install and Deploy Backend in your local machine
Once cloning processes are complete, you will have to install and deploy both back-end and front-end part in your local machine, so don't close the terminal, type: <br />
`cd bug_bounty` <br />
instead, this will take you to the back-end source. <br />
Now, you will be installing dependencies in 2 steps.Installation of first step will refer to modules that are required by our back-end source.*(Node.js)*
#### To install back-end `node_modules`,
   - First, make sure you are inside the **bug_bounty** directory
   
   - Then, type `npm install` to install the dependencies of back-end source
   

## Install and Deploy Frontend in your local machine
The second step of deployment will refer to the front-end *(React.js)* modules.<br /> 
#### 1. To install front-end `node_modules`,
   - First, make sure you are inside the **bug_bounty_client** directory
   
   - Then type `npm install` to install the dependencies of front-end source
        

Once the second npm install command finishes its job, check if the `node_modules` directory has been created inside both ***bug_bounty*** and ***bug_bounty_client*** folders of the project<br /><br />
After it's done, you can [proceed](#run-the-app-on-your-machine) to the next step.



## Run the app on your machine
- To run the app, go back to the **bug_bounty** folder, where our back-end source is located by typing `cd bug_bounty`

- Once you are, simply type:

`npm start`

- This script is set in ***package.json*** file of the repository folder of our project, it simply runs back-end starting scripts to get the app deployed on the local computer.

- As the final step, you will need also deploying the front-end source, go back to the **bug_bounty_client** folder by typing `cd ../bug_bounty_client`

- And just type `npm start`, the same process as the back-end part, this time it'll deploy the front-end.

#### ***Now that you have deployed both back-end and front-end servers, you are good to go.Enjoy surfing.***

import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import './Buttons.css'

class TransparentButton extends Component {
  constructor(props) {
    super(props)
    this.goAnchor = this.goAnchor.bind(this)
  }

  goAnchor() {
    const { history, baseUrl, anchor, location } = this.props
    if (!location.pathname.includes(baseUrl)) {
      history.push(baseUrl)
    }

    if (anchor)
      setTimeout(
        () =>
          document
            .getElementById(anchor)
            .scrollIntoView({ behavior: 'smooth' }),
        1000
      )
  }

  render() {
    const { history, title, href, btnstyle } = this.props
    return (
      <button className={btnstyle} onClick={this.goAnchor}>
        {title}
      </button>
    )
  }
}

export default withRouter(TransparentButton)

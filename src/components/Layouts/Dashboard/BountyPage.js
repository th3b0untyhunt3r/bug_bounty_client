import React, { Component } from 'react'
import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Typography,
  Container,
  withStyles,
  InputLabel,
  Step,
  Stepper,
  StepButton,
} from '@material-ui/core'

import StorageIcon from '@material-ui/icons/Storage'
import { withSnackbar } from 'material-ui-snackbar-provider'
import { encryptData, decryptData } from '../../../shared/helpers/RSA'

import {
  getBountyById,
  getPublicKey,
  createNewReport,
} from '../../../shared/helpers/Request'

import { encryptMessage, decryptMessage } from '../../../shared/helpers/RSA'
import Card from '../../Additional/Card'
import CardHeader from '../../Additional/CardHeader'
import CardBody from '../../Additional/CardBody'
import CardFooter from '../../Additional/CardFooter'
import GridContainer from '../../Additional/GridContainer'
import GridItem from '../../Additional/GridItem'
import CustomInput from '../../Additional/CustomInput'

const styles = {
  container: {
    backgroundColor: '#f5f5f5',
    borderRadius: '10px',
  },
  cardCategoryWhite: {
    color: 'rgba(255,255,255,.62)',
    margin: '0',
    fontSize: '14px',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginTop: '0',
    marginBottom: '0',
  },
  cardTitleWhite: {
    color: '#FFFFFF',
    marginTop: '0px',
    minHeight: 'auto',
    fontWeight: '300',
    fontSize: '20px',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: '3px',
    textDecoration: 'none',
  },
  sectionInfo: {
    color: '#AAAAAA',
    marginTop: '25px',
  },
  description: {
    lineHeight: 1.5,
  },
  flexDiv: {
    display: 'flex',
    justifyContent: 'center',
  },
  setIcon: {
    margin: '0px 10px 0px 0px',
  },
  montSerrat: {
    fontFamily: "'Montserrat', sans-serif",
  },
  margin5: {
    margin: '5px',
  },
  justifyRight: {
    justifyContent: 'center',
  },
  currentInfo: {
    fontSize: '25px',
  },
  privAcc: {
    marginLeft: '-15px',
    fontWeight: '400',
    color: '#AAAAAA',
  },
  verticCenter: {
    lineHeight: '35px',
  },
  bigBorder: {
    marginTop: '25px',
  },
  specialAnchor: {
    color: '#26c6da',
    textDecoration: 'none',
  },
  scrollArea: {
    overflowY: 'scroll',
    maxHeight: '250px',
  },
  onlyCenter: {
    textAlign: 'center',
  },
  submitButton: {
    background: 'linear-gradient(315deg, #045de9 0%, #09c6f9 74%)',
    borderRadius: 3,
    border: 0,
    color: 'white',
    height: 48,
    padding: '0 30px',
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
  },
}

function getSteps() {
  return ['LOW', 'MEDIUM', 'HIGH']
}

function HorizontalStepper(props) {
  const [activeStep, setActiveStep] = React.useState(0)
  const [completed] = React.useState({})
  const steps = getSteps()

  const handleStep = (step) => () => {
    setActiveStep(step)
  }

  function getStepContent(step) {
    switch (step) {
      case 0:
        return `${props.currency} ${props.low}`
      case 1:
        return `${props.currency} ${props.medium}`
      case 2:
        return `${props.currency} ${props.high}`
      default:
        return 'Unknown'
    }
  }

  return (
    <div style={{ textAlign: 'center' }}>
      <div style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
        <Stepper nonLinear activeStep={activeStep} style={{ width: '100%' }}>
          {steps.map((label, index) => (
            <Step key={label}>
              <StepButton
                icon=""
                onClick={handleStep(index)}
                completed={completed[index]}
              >
                {label}
              </StepButton>
            </Step>
          ))}
        </Stepper>
      </div>

      <Typography style={{ fontFamily: 'inherit', fontSize: '40px' }}>
        {getStepContent(activeStep)}
      </Typography>
    </div>
  )
}

class BountyPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isOpen: false,
      public_key: '',
      reportName: '',
      reportContent: '',
      bountyId: props.match.params.bountyId,
      currency: [],
      domains: [],
      company: [],
    }
    this.getData = this.getData.bind(this)
    this.submitReport = this.submitReport.bind(this)
  }

  componentDidMount() {
    this.getData()
  }

  handleClickOpen = () => {
    this.setState({ isOpen: true })
  }

  handleClose = () => {
    this.setState({ isOpen: false })
  }

  handleForm = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    })
  }

  getData() {
    const { bountyId } = this.state
    getBountyById(bountyId).then((response) => {
      this.setState({
        domains: response.bounty.domains,
        is_private: response.bounty.is_private,
        _id: response.bounty._id,
        name: response.bounty.name,
        policy: response.bounty.policy,
        company: response.bounty.company,
        low_price: response.bounty.low_price,
        medium_price: response.bounty.medium_price,
        high_price: response.bounty.high_price,
        currency: response.bounty.currency,
        created_at: response.bounty.created_at,
      })
      return getPublicKey(response.bounty.company._id).then((response) =>
        this.setState({ public_key: response.public_key })
      )
    })
  }

  async submitReport() {
    const {
      reportName,
      reportContent,
      public_key,
      bountyId,
      company,
    } = this.state

    const encryptedMessage = await encryptData(public_key, reportContent)
    createNewReport({
      name: reportName,
      content: encryptedMessage,
      bountyId,
      comId: company._id,
    })
      .then((response) => {
        this.props.snackbar.showMessage('Report successfully submitted!')
        this.handleClose()
      })
      .catch((err) => {
        var errorMessage = null
        if (Array.isArray(err.error)) errorMessage = err.error[0]
        else errorMessage = err.error

        this.props.snackbar.showMessage(
          errorMessage ? errorMessage : 'Please try again'
        )
      })
  }

  convertYear(date) {
    var formattedDate = new Date(date)
    return formattedDate.getFullYear()
  }
  convertMonth(date) {
    var formattedDate = new Date(date)
    return formattedDate.getMonth()
  }
  convertDay(date) {
    var formattedDate = new Date(date)
    return formattedDate.getDate()
  }

  render() {
    const { classes } = this.props
    const {
      company,
      currency,
      created_at,
      domains,
      is_private,
      low_price,
      medium_price,
      high_price,
      name,
      isOpen,
      policy,
      reportName,
      reportContent,
    } = this.state
    return (
      <Container maxWidth="lg" className={classes.container}>
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <Card>
              <CardHeader color="info">
                <div className={classes.flexDiv}>
                  <div className={classes.setIcon}>
                    <StorageIcon fontSize="large" />
                  </div>
                  <div className={classes.verticCenter}>
                    <h4 className={classes.cardTitleWhite}>
                      Bounty Page — {name}
                    </h4>
                  </div>
                </div>
              </CardHeader>

              <CardBody>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                    <GridContainer>
                      <GridItem xs={12} sm={12} md={12}>
                        <InputLabel className={classes.sectionInfo}>
                          LAUNCHED BY
                        </InputLabel>
                        <br />
                      </GridItem>
                      <GridItem xs={12} sm={12} md={12}>
                        <Typography variant="h5">{company.name}</Typography>
                      </GridItem>
                    </GridContainer>
                  </GridItem>

                  <GridItem xs={12} sm={12} md={4}>
                    <GridContainer>
                      <GridItem xs={12} sm={12} md={12}>
                        <InputLabel className={classes.sectionInfo}>
                          APPLIED DOMAINS
                        </InputLabel>
                        <br />
                      </GridItem>
                      <GridItem xs={12} sm={12} md={12}>
                        <Typography variant="h6">
                          {domains.map((domain) => {
                            return (
                              <a className={classes.specialAnchor}>
                                {domain.domain} <br />
                              </a>
                            )
                          })}
                        </Typography>
                      </GridItem>
                    </GridContainer>
                  </GridItem>

                  <GridItem xs={12} sm={12} md={4}>
                    <GridContainer>
                      <GridItem xs={12} sm={12} md={12}>
                        <InputLabel className={classes.sectionInfo}>
                          LAUNCH DATE
                        </InputLabel>
                        <br />
                      </GridItem>
                      <GridItem xs={12} sm={12} md={12}>
                        <Typography variant="h3">
                          {`${this.convertYear(created_at)}/${this.convertMonth(
                            created_at
                          )}/${this.convertDay(created_at)}`}
                        </Typography>
                      </GridItem>
                    </GridContainer>
                  </GridItem>
                </GridContainer>

                <GridContainer className={classes.bigBorder}>
                  <GridItem xs={12} sm={12} md={12}>
                    <InputLabel className={classes.sectionInfo}>
                      PROGRAM POLICY
                    </InputLabel>
                    <br />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={12}>
                    <Typography className={classes.scrollArea} variant="body1">
                      {policy}
                    </Typography>
                  </GridItem>
                </GridContainer>

                <GridContainer style={{ justifyContent: 'center' }}>
                  <GridItem xs={12} sm={12} md={12}>
                    <InputLabel className={classes.sectionInfo}>
                      PRICE RANGE FOR BOUNTIES
                    </InputLabel>
                    <br />
                  </GridItem>
                  <GridItem xs={6}>
                    <HorizontalStepper
                      currency={currency.short_name}
                      low={low_price}
                      medium={medium_price}
                      high={high_price}
                    />
                  </GridItem>
                </GridContainer>
              </CardBody>

              <CardFooter style={{ display: 'flex', justifyContent: 'center' }}>
                <Button
                  onClick={this.handleClickOpen}
                  className={classes.submitButton}
                >
                  Submit Report To This Bounty
                </Button>
                <Dialog
                  open={isOpen}
                  onClose={this.handleClose}
                  fullScreen={true}
                  aria-labelledby="form-dialog-title"
                >
                  <DialogTitle id="form-dialog-title">Report</DialogTitle>
                  <DialogContent>
                    <DialogContentText>
                      Please write name and content of your report.
                    </DialogContentText>
                    <TextField
                      autoFocus
                      margin="dense"
                      id="name"
                      name="reportName"
                      label="Report Name"
                      onChange={this.handleForm}
                      fullWidth
                    />
                    <TextField
                      autoFocus
                      margin="dense"
                      id="content"
                      multiline={true}
                      name="reportContent"
                      label="Report Content"
                      onChange={this.handleForm}
                      fullWidth
                    />
                  </DialogContent>
                  <DialogActions>
                    <Button onClick={this.handleClose} color="primary">
                      Cancel
                    </Button>
                    <Button onClick={this.submitReport} color="primary">
                      Submit
                    </Button>
                  </DialogActions>
                </Dialog>
              </CardFooter>
            </Card>
          </GridItem>

          {/* <GridItem xs={12} sm={12} md={5}>
            <Card profile>
              <CardAvatar profile>
                <a>
                  <img />
                </a>
              </CardAvatar>
              <CardBody profile>
                <h3
                  style={{ fontWeight: '550', color: '#716b68' }}
                  className={classes.cardCategory}
                >
                  barabara
                </h3>
                <h1 className={classes.cardTitle}>barabara</h1>
                <p className={classes.description}>barabara</p>
                <Button variant="contained" color="secondary">
                  {`SUBMITTED REPORTS (14389)`}
                </Button>
              </CardBody>
            </Card>
          </GridItem> */}
        </GridContainer>
      </Container>
    )
  }
}

export default withSnackbar()(withStyles(styles)(BountyPage))

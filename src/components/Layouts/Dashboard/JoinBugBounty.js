import React, { Component } from 'react'
import {
  Container,
  Grid,
  Typography,
  TextField,
  Checkbox,
  FormControlLabel,
  InputLabel,
  Input,
  ListItemText,
  Select,
  MenuItem,
  Tooltip,
} from '@material-ui/core'

import LoadingButton from '../../Buttons/LoadingButton'
import HelpIcon from '@material-ui/icons/Help'
import {
  getDomainsByUser,
  getAllCurrencies,
  createBounty,
} from '../../../shared/helpers/Request'

import { withSnackbar } from 'material-ui-snackbar-provider'

const initialState = {
  name: '',
  policy: '',
  low_price: 0,
  medium_price: 0,
  high_price: 0,
  domains: [],
  currency: '',
  all_currencies: [],
  all_domains: [],
  is_private: false,
}

class JoinBugBounty extends Component {
  constructor(props) {
    super(props)
    this.state = initialState
    this.loadData = this.loadData.bind(this)
  }

  componentDidMount() {
    this.loadData()
  }

  loadData = () => {
    getDomainsByUser().then((data) => {
      this.setState({ all_domains: data.domains })
    })
    getAllCurrencies().then((data) => {
      this.setState({ all_currencies: data.currencies })
    })
  }

  submitForm = async () => {
    var requestBody = this.state
    delete requestBody['all_currencies']
    delete requestBody['all_domains']
    return createBounty(requestBody)
      .then((response) => {
        this.setState(initialState)
        this.props.snackbar.showMessage('Bounty program successfully created')
      })
      .catch((err) => {
        var errorMessage = null
        if (Array.isArray(err.error)) errorMessage = err.error[0]
        else errorMessage = err.error

        this.props.snackbar.showMessage(
          errorMessage ? errorMessage : 'Please try again'
        )
      })
  }

  handleForm = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    })
  }

  handleCheckbox = (event) => {
    this.setState({
      [event.target.name]: event.target.checked,
    })
  }

  render() {
    const { all_domains, all_currencies, domains, currency } = this.state
    return (
      <Container
        maxWidth="md"
        style={{
          background: '#fff',
          height: '81vh',
          borderRadius: '25px',
          padding: '50px 25px',
          marginBottom: '8px',
          direction: 'column',
          alignItems: 'center',
          justifyItems: 'center',
        }}
        fixed
      >
        <React.Fragment>
          <Typography variant="h6" gutterBottom>
            Create Program
          </Typography>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="name"
                name="name"
                label="Bounty Name"
                fullWidth
                onChange={this.handleForm}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <InputLabel id="mutiple-checkbox-label">Domains</InputLabel>
              <Select
                labelId="mutiple-checkbox-label"
                id="mutiple-checkbox"
                name="domains"
                multiple={true}
                value={domains}
                fullWidth
                onChange={this.handleForm}
                input={<Input />}
                renderValue={(selected) =>
                  all_domains
                    .filter((domain) => selected.includes(domain._id))
                    .map((domain) => domain.domain)
                    .join(', ')
                }
              >
                {all_domains.map((domain) => (
                  <MenuItem key={domain._id} value={domain._id}>
                    <Checkbox checked={domains.indexOf(domain._id) > -1} />
                    <ListItemText primary={domain.domain} />
                  </MenuItem>
                ))}
              </Select>
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                id="policy"
                name="policy"
                label="Program Policy"
                fullWidth
                onChange={this.handleForm}
                multiline={true}
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                required
                id="low_price"
                name="low_price"
                label="Low Price"
                type="number"
                fullWidth
                onChange={this.handleForm}
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                required
                id="medium_price"
                name="medium_price"
                label="Medium Price"
                type="number"
                fullWidth
                onChange={this.handleForm}
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                required
                id="high_price"
                name="high_price"
                label="High Price"
                type="number"
                fullWidth
                onChange={this.handleForm}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <InputLabel id="simple-select-label">Currency</InputLabel>
              <Select
                labelId="simple-select-label"
                id="simple-select"
                name="currency"
                value={currency}
                onChange={this.handleForm}
              >
                {all_currencies.map((currency) => (
                  <MenuItem key={currency._id} value={currency._id}>
                    {currency.short_name}
                  </MenuItem>
                ))}
              </Select>
            </Grid>
            <Grid item xs={12}>
              <FormControlLabel
                control={<Checkbox color="secondary" />}
                label="Private"
                name="is_private"
                onChange={this.handleCheckbox}
              />
              <Tooltip title="Private bug bounty will be seen by only PRO hackers and etc">
                <HelpIcon />
              </Tooltip>
            </Grid>
            <Grid item xs={12}>
              <LoadingButton title="Create" sendRequest={this.submitForm} />
            </Grid>
          </Grid>
        </React.Fragment>
      </Container>
    )
  }
}

export default withSnackbar()(JoinBugBounty)

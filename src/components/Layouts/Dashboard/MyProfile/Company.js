import React, { Component } from 'react'

import {
  Switch,
  Button,
  InputLabel,
  withStyles,
  FormControlLabel,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '@material-ui/core'
import MaterialTable from 'material-table'
import { withSnackbar } from 'material-ui-snackbar-provider'
import LoadingButton from '../../../Buttons/LoadingButton'
import SettingsIcon from '@material-ui/icons/Settings'
import {
  getResponsibleInfo,
  getCompanyInfo,
  createDomain,
  verifyDomain,
} from '../../../../shared/helpers/Request'

import GridItem from '../../../Additional/GridItem'
import GridContainer from '../../../Additional/GridContainer'
import CustomInput from '../../../Additional/CustomInput'
import Card from '../../../Additional/Card'
import CardHeader from '../../../Additional/CardHeader'
import CardAvatar from '../../../Additional/CardAvatar'
import CardBody from '../../../Additional/CardBody'
import CardFooter from '../../../Additional/CardFooter'

import ROUTES from '../../../../shared/helpers/Constants'

const columns = [
  { title: 'Domain', field: 'domain' },
  { title: 'Verification Key', field: 'verification_key' },
  { title: 'Status', field: 'status' },
]

const styles = {
  cardCategoryWhite: {
    color: 'rgba(255,255,255,.62)',
    margin: '0',
    fontSize: '14px',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginTop: '0',
    marginBottom: '0',
  },
  cardTitleWhite: {
    color: '#FFFFFF',
    marginTop: '0px',
    minHeight: 'auto',
    fontWeight: '300',
    fontSize: '20px',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: '3px',
    textDecoration: 'none',
  },
  sectionInfo: {
    color: '#AAAAAA',
    marginTop: '25px',
  },
  description: {
    lineHeight: 1.5,
  },
  flexDiv: {
    display: 'flex',
  },
  setIcon: {
    margin: '0px 10px 0px 0px',
  },
  montSerrat: {
    fontFamily: "'Montserrat', sans-serif",
  },
  margin5: {
    margin: '5px',
  },
  justifyRight: {
    justifyContent: 'center',
  },
  currentInfo: {
    fontSize: '25px',
  },
  privAcc: {
    marginLeft: '-15px',
    fontWeight: '400',
    color: '#AAAAAA',
  },
}

class Company extends Component {
  constructor(props) {
    super(props)
    this.state = {
      first_name: '',
      last_name: '',
      email: '',
      position: '',
      domains: [],
      description: '',
      logo: '',
      linkedin: '',
      facebook: '',
      instagram: '',
      name: '',
      address: '',
      contact_number: 0,
      email: '',
      isOpen: false,
      staticCompanyInfo: {
        logo: '',
        name: '',
        status: '',
        description: '',
      },
    }
    this.loadData = this.loadData.bind(this)
  }

  componentDidMount() {
    this.loadData()
  }

  loadData() {
    getResponsibleInfo().then((response) =>
      this.setState({
        first_name: response.responsible.first_name,
        last_name: response.responsible.last_name,
        email: response.responsible.email,
        position: response.responsible.position,
      })
    )
    getCompanyInfo().then((response) =>
      this.setState({
        domains: response.company.domains,
        description: response.company.description,
        linkedin: response.company.linkedin,
        facebook: response.company.facebook,
        instagram: response.company.instagram,
        name: response.company.name,
        contact_number: response.company.contact_number,
        com_email: response.company.email,
        staticCompanyInfo: {
          logo: response.company.logo,
          name: response.company.name,
          status: response.company.status,
          description: response.company.description,
        },
      })
    )
  }

  handleForm = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    })
  }

  handleSwitch = () => {
    this.setState({
      is_private: !this.state.is_private,
    })
  }

  submitForm = (event) => {
    console.log(this.state)
  }

  handleClickOpen = () => {
    this.setState({ isOpen: true })
  }

  handleClose = () => {
    this.setState({ isOpen: false })
    this.loadData()
  }

  render() {
    const { classes } = this.props
    const {
      first_name,
      last_name,
      email,
      position,
      domains,
      description,
      logo,
      linkedin,
      facebook,
      instagram,
      name,
      address,
      contact_number,
      com_email,
      staticCompanyInfo,
      isOpen,
    } = this.state
    console.log(this.state)

    return (
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12} md={7}>
            <Card>
              <CardHeader color="danger">
                <div className={classes.flexDiv}>
                  <div className={classes.setIcon}>
                    <SettingsIcon fontSize="large" />
                  </div>
                  <div>
                    <h4 className={classes.cardTitleWhite}>Edit Profile</h4>
                    <p className={classes.cardCategoryWhite}>
                      Complete your profile
                    </p>
                  </div>
                </div>
              </CardHeader>
              <CardBody>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={12}>
                    <InputLabel className={classes.sectionInfo}>
                      RESPONSIBLE INFORMATION
                    </InputLabel>
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                    <CustomInput
                      labelText="First name"
                      id="first_name"
                      inputProps={{
                        name: 'first_name',
                        value: first_name,
                        onChange: this.handleForm,
                      }}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <CustomInput
                      labelText="Last name"
                      id="last_name"
                      formControlProps={{
                        fullWidth: true,
                      }}
                      inputProps={{
                        name: 'last_name',
                        value: last_name,
                        onChange: this.handleForm,
                      }}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <CustomInput
                      labelText="Email"
                      id="email"
                      formControlProps={{
                        fullWidth: true,
                      }}
                      inputProps={{
                        name: 'email',
                        value: email,
                        onChange: this.handleForm,
                      }}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <CustomInput
                      labelText="Position"
                      id="position"
                      formControlProps={{
                        fullWidth: true,
                      }}
                      inputProps={{
                        name: 'position',
                        value: position,
                        onChange: this.handleForm,
                      }}
                    />
                  </GridItem>
                </GridContainer>

                <GridContainer>
                  <GridItem xs={12} sm={12} md={12}>
                    <InputLabel className={classes.sectionInfo}>
                      COMPANY INFORMATION
                    </InputLabel>
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={3}>
                    <CustomInput
                      labelText="Name"
                      id="name"
                      inputProps={{
                        name: 'name',
                        onChange: this.handleForm,
                        value: name,
                      }}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                    <CustomInput
                      labelText="Address"
                      id="address"
                      inputProps={{
                        name: 'address',
                        onChange: this.handleForm,
                        value: address,
                      }}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                    <CustomInput
                      labelText="Email"
                      id="email"
                      inputProps={{
                        name: 'com_email',
                        value: com_email,
                        onChange: this.handleForm,
                      }}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                    <CustomInput
                      labelText="Contact Number"
                      id="contact-number"
                      inputProps={{
                        name: 'contact_number',
                        value: contact_number,
                        onChange: this.handleForm,
                      }}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={12}>
                    <CustomInput
                      labelText="Description"
                      id="description"
                      inputProps={{
                        name: 'description',
                        value: description,
                        onChange: this.handleForm,
                      }}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                  </GridItem>
                </GridContainer>

                <GridContainer>
                  <GridItem xs={12} sm={12} md={12}>
                    <InputLabel className={classes.sectionInfo}>
                      SOCIAL ACCOUNTS
                    </InputLabel>
                  </GridItem>
                </GridContainer>

                <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                    <CustomInput
                      value={facebook}
                      labelText="Facebook"
                      id="facebook"
                      inputProps={{
                        name: 'facebook',
                        value: facebook,
                        onChange: this.handleForm,
                      }}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <CustomInput
                      labelText="LinkedIn"
                      id="linkedin"
                      inputProps={{
                        name: 'linkedin',
                        value: linkedin,
                        onChange: this.handleForm,
                      }}
                      formControlProps={{
                        fullWidth: true,
                      }}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <CustomInput
                      labelText="Instagram"
                      id="instagram"
                      formControlProps={{
                        fullWidth: true,
                      }}
                      inputProps={{
                        name: 'instagram',
                        value: instagram,
                        onChange: this.handleForm,
                      }}
                    />
                  </GridItem>
                </GridContainer>
              </CardBody>
              <CardFooter>
                <Button
                  variant="outlined"
                  color="secondary"
                  onClick={this.setDialogOpen}
                >
                  Save & Update Profile
                </Button>
              </CardFooter>
            </Card>
          </GridItem>

          <GridItem xs={12} sm={12} md={5}>
            <Card profile>
              <CardAvatar profile>
                <a
                  href={`${ROUTES.devURL}/image/logo/${staticCompanyInfo.logo}`}
                  onClick={(e) => e.preventDefault()}
                >
                  <img
                    src={`${ROUTES.devURL}/image/logo/${staticCompanyInfo.logo}`}
                    alt={staticCompanyInfo.name}
                  />
                </a>
              </CardAvatar>
              <CardBody profile>
                <h3
                  style={{ fontWeight: '550', color: '#716b68' }}
                  className={classes.cardCategory}
                >
                  {`${staticCompanyInfo.name} | ${staticCompanyInfo.status}`}
                </h3>
                <h1
                  className={classes.cardTitle}
                >{`${staticCompanyInfo.name} • 0`}</h1>
                <p
                  className={classes.description}
                >{`${staticCompanyInfo.description}`}</p>
                <Button variant="contained" color="secondary">
                  {`Bounties`}
                </Button>
              </CardBody>
            </Card>

            {/* <Card profile>
              <GridContainer style={{ marginTop: '18px' }}>
                <GridItem xs={12} sm={12} md={12}>
                  <Typography
                    variant="subtitle2"
                    className={classes.currentInfo}
                  >
                    Current Profile Info
                  </Typography>
                </GridItem>
              </GridContainer>

              <GridContainer>
                <GridItem xs={12} sm={12} md={6}>
                  <CustomInput
                    labelText={`${hacker['username']} (Username)`}
                    id="username"
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      disabled: true,
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <CustomInput
                    labelText={`${hacker['email']} (E-mail Address)`}
                    id="email"
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      disabled: true,
                    }}
                  />
                </GridItem>
              </GridContainer>

              <GridContainer>
                <GridItem xs={12} sm={12} md={4}>
                  <CustomInput
                    labelText={`${hacker['voen']} (VOEN)`}
                    id="voen"
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      disabled: true,
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={8}>
                  <CustomInput
                    labelText={`${hacker['home_address']} (Home Address)`}
                    id="home"
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      disabled: true,
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <CardFooter className={classes.justifyRight}>
                    <Button
                      justIcon
                      color="transparent"
                      className={classes.margin5}
                      href={`${hacker['facebook']}`}
                      target="_blank"
                    >
                      <FacebookIcon fontsize="small" />
                    </Button>
                    <Button
                      justIcon
                      color="transparent"
                      className={classes.margin5}
                      href={`${hacker['instagram']}`}
                      target="_blank"
                    >
                      <InstagramIcon fontsize="small" />
                    </Button>
                    <Button
                      justIcon
                      color="transparent"
                      className={classes.margin5}
                      href={`${hacker['linkedin']}`}
                      target="_blank"
                    >
                      <LinkedInIcon fontsize="small" />
                    </Button>
                  </CardFooter>
                </GridItem>
              </GridContainer>
            </Card> */}
          </GridItem>
          <GridItem md={12}>
            <MaterialTable
              title="Domains"
              columns={columns}
              data={domains}
              options={{
                actionsColumnIndex: -1,
              }}
              actions={[
                {
                  icon: 'verified',
                  tooltip: 'Verify Domain',
                  onClick: (event, rowData) =>
                    verifyDomain(rowData._id)
                      .then((data) => {
                        this.props.snackbar.showMessage(
                          'Domain successfully verified'
                        )
                        this.loadData()
                      })
                      .catch((err) => {
                        if (Array.isArray(err.error))
                          this.props.snackbar.showMessage(err.error[0])
                        else this.props.snackbar.showMessage(err.error)
                      }),
                },
                {
                  icon: 'delete',
                  tooltip: 'Delete Domain',
                  onClick: (event, rowData) => console.log(rowData),
                },
                {
                  icon: 'add',
                  tooltip: 'Add Domain',
                  isFreeAction: true,
                  onClick: (event) => this.handleClickOpen(),
                },
              ]}
            />
            <DomainVerificationDialog
              handleClose={this.handleClose}
              isOpen={isOpen}
              snackbar={this.props.snackbar}
            />
          </GridItem>
        </GridContainer>
      </div>
    )
  }
}

class DomainVerificationDialog extends Component {
  constructor(props) {
    super(props)
    this.state = {
      domain: '',
      isFirstStage: true,
      verificationKey: null,
    }
  }

  sendCreateDomainRequest = () => {
    const { domain } = this.state
    return createDomain({ domain })
      .then((response) =>
        this.setState({
          isFirstStage: false,
          verificationKey: response.domainModel.verification_key,
        })
      )
      .catch((err) => {
        if (Array.isArray(err.error))
          this.props.snackbar.showMessage(err.error[0])
        else this.props.snackbar.showMessage(err.error)
      })
  }

  handleForm = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    })
  }

  render() {
    const { handleClose, isOpen } = this.props

    const { isFirstStage, verificationKey } = this.state
    return (
      <Dialog
        open={isOpen}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
        maxWidth="md"
      >
        {isFirstStage ? (
          <div>
            {' '}
            <DialogTitle id="form-dialog-title">Add Domain</DialogTitle>
            <DialogContent>
              <DialogContentText>
                Please enter a new domain (e.g. www.example.com)
              </DialogContentText>
              <TextField
                autoFocus
                margin="dense"
                id="domain"
                name="domain"
                label="Domain"
                fullWidth
                onChange={this.handleForm}
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose} color="primary">
                Cancel
              </Button>
              <LoadingButton
                sendRequest={this.sendCreateDomainRequest}
                title="Add"
              />
            </DialogActions>
          </div>
        ) : (
          <DialogContent>
            <DialogContentText>
              Verification key for this domain is {verificationKey}. Please add
              this domain to your DNS and press verify button.
            </DialogContentText>
          </DialogContent>
        )}
      </Dialog>
    )
  }
}

export default withSnackbar()(withStyles(styles)(Company))

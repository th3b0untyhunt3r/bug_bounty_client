import React, { Component } from 'react'
import { getHackerDB, updateHackerDB } from '../../../shared/helpers/Request'
import { Container } from '@material-ui/core'
import Hacker from './MyProfile/Hacker'

class MyProfileHacker extends Component {
  render() {
    return (
      <Container maxWidth="lg">
        <Hacker getData={getHackerDB} putData={updateHackerDB} />
      </Container>
    )
  }
}

export default MyProfileHacker

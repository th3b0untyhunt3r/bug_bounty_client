import React, { Component } from 'react'
import Container from '@material-ui/core/Container'
import ServerPaginationTable from '../../Tables/ServerPaginationTable'
import { getHackerLeaderboard } from '../../../shared/helpers/Request'
import User from '../../Tables/Rows/User'

class Leaderboard extends Component {
  render() {
    return (
      <Container
        maxWidth="md"
        style={{
          background: '#fff',
          height: '79vh',
          borderRadius: '25px',
          padding: '0px 25px 0px 25px',
          marginBottom: '8px',
          direction: 'column',
          alignItems: 'center',
          justifyItems: 'center',
        }}
        fixed
      >
        <div style={{ padding: 'auto' }}>
          <ServerPaginationTable
            getData={getHackerLeaderboard}
            rowsPerPage={10}
          >
            <User />
          </ServerPaginationTable>
        </div>
      </Container>
    )
  }
}

export default Leaderboard

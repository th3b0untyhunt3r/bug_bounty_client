import React from 'react'
import { makeStyles } from '@material-ui/core/styles'

import CreateIcon from '@material-ui/icons/Create'
import LaunchIcon from '@material-ui/icons/Launch'
import VerifiedUser from '@material-ui/icons/VerifiedUser'
import HourglassFullIcon from '@material-ui/icons/HourglassFull'
import VisibilityIcon from '@material-ui/icons/Visibility'
import PaymentIcon from '@material-ui/icons/Payment'

import GridContainer from '../../../Additional/GridContainer'
import GridItem from '../../../Additional/GridItem'
import InfoArea from '../../../Additional/InfoArea'

const title = {
  color: '#FFFFFF',
  margin: '1.75rem 0 0.875rem',
  textDecoration: 'none',
  fontWeight: '2000',
  fontFamily: `"Montserrat", sans-serif`,
}

const productStyle = {
  section: {
    backgroundColor: '#292929',
    padding: '20px 200px',
    textAlign: 'center',
    color: '#F5F0F0',
  },
  title: {
    ...title,
    marginBottom: '1rem',
    marginTop: '30px',
    minHeight: '32px',
    textDecoration: 'none',
  },
  description: {
    // color: "#F5F0F0"
  },
}

const useStyles = makeStyles(productStyle)

export default function ProductSection() {
  const classes = useStyles()
  return (
    <div className={classes.section}>
      <GridContainer justify="center">
        <GridItem xs={12} sm={12} md={8}>
          <h2 style={{ fontSize: '3rem' }} className={classes.title}>
            How it works
          </h2>
          <h3 className={classes.description}>
            Learn more by following the below instructions on getting started in
            our bug bounty platform. Being easy-to-use and totally free, our
            platform offers a number of options to keep your company secure &
            safe.
          </h3>
        </GridItem>
      </GridContainer>
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="Get Started"
              description="Choose a suitable responsible for your company and give him/her the proper instructions on how to register your company in our platform"
              icon={CreateIcon}
              iconColor="info"
              vertical
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="Verify your Domains"
              description="Verify at least a domain or two to tell hackers where to start testing your (web) application and add these domains in the policy"
              icon={VerifiedUser}
              iconColor="success"
              vertical
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="Launch a Program"
              description="Create your first bug bounty program of your choosing and light it up along with its new policy.Programs can be public or private of your choice"
              icon={LaunchIcon}
              iconColor="warning"
              vertical
            />
          </GridItem>
        </GridContainer>
      </div>
      <div style={{ paddingTop: '150px' }}>
        <GridContainer>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="Wait for it"
              description="Patience is the most important thing lots of people lack.Do not despair while hackers do their work — Hack for good!"
              icon={HourglassFullIcon}
              iconColor="gray"
              vertical
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="Review reports"
              description="Review submitted reports of your program(s) and mark them correspondingly; approved, duplicate or non-existing bug"
              icon={VisibilityIcon}
              iconColor="rose"
              vertical
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="Give the devil his due"
              description="After you considered a report valid, consult the payment together and enjoy having your app more secure than before"
              icon={PaymentIcon}
              iconColor="danger"
              vertical
            />
          </GridItem>
        </GridContainer>
      </div>
    </div>
  )
}

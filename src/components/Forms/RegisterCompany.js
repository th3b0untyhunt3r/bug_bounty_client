import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import Avatar from '@material-ui/core/Avatar'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import Link from '@material-ui/core/Link'
import Grid from '@material-ui/core/Grid'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import LoadingButton from '../Buttons/LoadingButton'
import { withSnackbar } from 'material-ui-snackbar-provider'

const styles = (theme) => ({
  paper: {
    marginTop: theme.spacing(2),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '70%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
})

class RegisterCompany extends Component {
  constructor(props) {
    super(props)
    const { postData, status } = props
    this.state = {
      responsible: {
        first_name: '',
        last_name: '',
        emailRes: '',
        password: '',
        passwordConfirmation: '',
        position: '',
      },
      company: {
        name: '',
        address: '',
        // description: '',
        contact_number: '',
        emailCom: '',
      },
      postData: postData,
      status: status,
      termsAccepted: false,
    }
  }

  setTermsAccepted = () => {
    this.setState({ termsAccepted: !this.state.termsAccepted })
  }

  handleForm = (event) => {
    const keyValue = event.target.name
    console.log(keyValue)
    if (keyValue.split('.')[0] === 'responsible')
      this.setState({
        responsible: {
          ...this.state.responsible,
          [keyValue.split('.')[1]]: event.target.value,
        },
      })
    else {
      this.setState({
        company: {
          ...this.state.company,
          [keyValue.split('.')[1]]: event.target.value,
        },
      })
    }
  }

  submitForm = async () => {
    const { postData, status } = this.state
    return postData(this.state)
      .then(() => {
        this.props.history.push('/postregister')
      })
      .catch((err) => {
        var errorMessage = null
        if (Array.isArray(err.error)) errorMessage = err.error[0]
        else errorMessage = err.error

        this.props.snackbar.showMessage(
          errorMessage ? errorMessage : 'Please try again'
        )
      })
  }

  render() {
    const { classes } = this.props
    const {
      first_name,
      last_name,
      emailRes,
      password,
      passwordConfirmation,
      position,
    } = this.state.responsible

    const {
      name,
      address,
      // description,
      contact_number,
      emailCom,
    } = this.state.company

    return (
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Register
        </Typography>
        <form className={classes.form} noValidate>
          <Typography component="h1" variant="subtitle2">
            Responsible Info
          </Typography>

          <Grid container spacing={2}>
            <Grid item md={12} lg={6}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="filled-basic"
                label="First Name"
                name="responsible.first_name"
                value={first_name}
                onChange={this.handleForm}
                autoFocus
              />
            </Grid>
            <Grid item md={12} lg={6}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="last_name"
                label="Last Name"
                name="responsible.last_name"
                value={last_name}
                onChange={this.handleForm}
              />
            </Grid>
            <Grid item md={12}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="emailRes"
                label="E-mail Address"
                helperText="Please be aware that this e-mail is to verify the company account and won't be displayed afterwards."
                name="responsible.emailRes"
                value={emailRes}
                onChange={this.handleForm}
              />
            </Grid>
            <Grid item md={12} lg={4}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="responsible.password"
                value={password}
                onChange={this.handleForm}
                label="Password"
                type="password"
                id="password"
              />
            </Grid>
            <Grid item md={12} lg={4}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="responsible.passwordConfirmation"
                value={passwordConfirmation}
                onChange={this.handleForm}
                label="Re-enter your Password"
                type="password"
                id="passwordConfirmation"
              />
            </Grid>
            <Grid item md={12} lg={4}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="position"
                label="Position in the Company"
                name="responsible.position"
                value={position}
                onChange={this.handleForm}
              />
            </Grid>
          </Grid>

          <Typography component="h1" variant="subtitle2">
            Company Info
          </Typography>

          <Grid container spacing={2}>
            <Grid item md={12} lg={4}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="name"
                label="Name"
                name="company.name"
                value={name}
                onChange={this.handleForm}
              />
            </Grid>
            <Grid item md={12} lg={4}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="address"
                label="Address"
                name="company.address"
                value={address}
                onChange={this.handleForm}
              />
            </Grid>
            <Grid item md={12} lg={4}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="contact_number"
                label="Contact Number"
                name="company.contact_number"
                value={contact_number}
                onChange={this.handleForm}
              />
            </Grid>
            <Grid item md={12}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="emailCom"
                label="E-mail address"
                helperText="This will be shown as company's public e-mail address and won't be used in verification process."
                name="company.emailCom"
                value={emailCom}
                onChange={this.handleForm}
              />
            </Grid>
            {/* <Grid item md={12}>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="description"
                                label="Describe Your Company"
                                name="description"
                                value={description}
                                onChange={this.handleForm}
                            />
                        </Grid> */}

            <Grid container justify="center">
              <Grid item>
                <Link target="_blank" href="/policy" variant="body2">
                  {'Read the full Policy of Team SecUrZone Here'}
                </Link>
              </Grid>
            </Grid>

            <Grid container justify="center">
              <Grid item>
                <FormControlLabel
                  control={<Checkbox justify="center" color="primary" />}
                  label="I agree to the Terms and Conditions"
                  onChange={this.setTermsAccepted}
                />
              </Grid>
            </Grid>

            <div style={{ width: '100%' }}>
              <LoadingButton
                title="Register Your Account"
                sendRequest={this.submitForm}
              />
            </div>
            <Grid container justify="flex-end">
              <Grid item>
                <Link href="/landing/login" variant="body2">
                  {'Already have an account? Sign In'}
                </Link>
              </Grid>
            </Grid>
          </Grid>
        </form>
      </div>
    )
  }
}

export default withSnackbar()(withStyles(styles)(withRouter(RegisterCompany)))
